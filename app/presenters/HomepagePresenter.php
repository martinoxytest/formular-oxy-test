<?php

/**
 * Soubor s presenterem pro akci "Homepage".
 * Na homepage je formulář pro odeslání dat k nově registrovanému uživateli
 * a výpis registrovaných uživatelů, který se načítá z exportu
 * na adrese http://www.sluzba.oxy.lcz/userslist?securekey=x5yW78q9
 *
 * @package  OXY - sluzba
 * @subpackage  Presenters
 * @author   Martin Marek <martin.marek@impnet.cz>
 * @version  $Revision: 1.0 $
 */

namespace App\Presenters;

use Nette,
    App\Model,
    Tracy\ILogger,
    Tracy\Debugger,
    Nette\Application\UI\Form;    

/**
 * Presenter pro homepage.
 * Na homepage je formulář pro odeslání dat k nově registrovanému uživateli
 * a výpis registrovaných uživatelů, který se načítá z exportu
 * na adrese http://www.sluzba.oxy.lcz/userslist?securekey=x5yW78q9
 *
 * @package  OXY - sluzba
 * @author   Martin Marek <martin.marek@impnet.cz>
 * @version  $Revision: 1.0 $
 */
final class HomepagePresenter extends Nette\Application\UI\Presenter
{
  public function __construct()
  {
    parent::__construct();
  }

  // Metoda pro vytvoření formuláře
  protected function createComponentRegistrationForm()
  {
    $form = new Form;

    $form->addGroup( 'a' );
    
    $form->addText('name', 'Jméno:')
         ->setRequired('Toto políčko je povinné.');
    
    $form->addPassword( 'password', 'Heslo:' )
         ->setRequired('Toto políčko je povinné.')
         ->addRule( Form::MIN_LENGTH, 'Minimální délka hesla je 6 znaků.', 6 );

    $form->addPassword( 'passwordAgain', 'Heslo znovu:' )
         ->setRequired('Toto políčko je povinné.')
         ->addRule( Form::MIN_LENGTH, 'Minimální délka hesla je 6 znaků.', 6 )
         ->addRule( Form::EQUAL, 'Hesla se neshodují.', $form['password'] );

    $form->addText('email', 'E-mail:')
         ->addRule( Form::EMAIL, 'Zadejte prosím správný formát e-mailové adresy.' )
         ->setRequired('Toto políčko je povinné.');

    $form->addSelect('role', 'Oprávnění:',
    [
      0 => 'uživatel',
      1 => 'administrátor',
    ]);

    $form->addGroup( 'b' );
    
    $form->addSubmit('add', 'Registrovat');
    
    $form->onSuccess[] = [$this, 'registrationFormSucceeded'];
    
    return $form;
  }

  // Metoda pro zpracování formuláře
  public function registrationFormSucceeded( Form $form )
  {
    $values = $form->getValues();

    $options = array(
      'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query( $values ),
      ),
    );

    $context = stream_context_create( $options );
    $json    = file_get_contents( 'http://www.sluzba.oxy.lcz/insertuser?securekey=x5yW78q9', false, $context );
    $return  = json_decode( $json,true );

    Debugger::log( $return );

    $this->flashMessage( 'Registrace byla úspěšně dokončena.' );
    $this->redirect( 'Homepage:' );
  }  

  protected function beforeRender()
  {
      $this->template->addFilter( 'userRoleToString', function ( $userRole )
      {
        // pokud je hodnota role = 0, jedná se běžného uživatele 
        if ( !$userRole ) 
          return 'uživatel';

        // pokud je hodnota role = 1, uživatel je administrátor
        if ( $userRole )
          return 'administrator';
      });
  }

  public function actionDefault()
  {
    $registeredUsersList = array();
    
    $feed = file_get_contents('http://www.sluzba.oxy.lcz/userslist?securekey=x5yW78q9');
    $xml  = simplexml_load_string( $feed, null, LIBXML_NOCDATA );

    if ( $xml != false )
    {
      $json = json_encode( $xml );

      $usersList = json_decode( $json,TRUE );     

      $iterator = 0;
  
      foreach( $usersList as $users )
      {
        foreach( $users as $user )
        {
          //Debugger::barDump( $user );

          $registeredUsersList[$iterator]['name'] = $user['NAME'];
          $registeredUsersList[$iterator]['email'] = $user['EMAIL'];
          $registeredUsersList[$iterator]['role'] = $user['ROLE'];

          $iterator++;
        }
      }
    }

    $this->template->registeredUsersList = $registeredUsersList;
  }

}
